<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
			try{
				$this->db= new PDO('mysql:host=localhost;dbname=books','root','');
				$this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			}catch(Exception $e){
				echo "Feil ved kobling til db";
				$e->getMessage();
			}
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();		
		try{
			$query=$this->db->prepare('SELECT * FROM books');
			$query->execute();
			$rows=$query->fetchAll(PDO::FETCH_ASSOC);
			foreach($rows as $row){
				$booklist[]= new Book($row['Title'], $row['Author'], $row['Description'],$row['ID']);				
			}
			return $booklist;
		}catch(Exception $e){
			echo "Feil ved henting av bokliste";
			$e->getMessage();
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		//echo var_dump($id);
        try{
			$query=$this->db->prepare('SELECT * FROM books WHERE ID = :num');
			$query->bindValue(':num', $id, PDO::PARAM_STR);
			$query->execute();
			$nyBook= $query->fetchAll(PDO::FETCH_ASSOC);
			$book = new Book($nyBook[0]['Title'], $nyBook[0]['Author'], $nyBook[0]['Description'], $nyBook[0]['ID']);
			return $book;
			}
		catch(Exception $e){
			echo "Feil ved henting av bok gjennom ID";
			$e->getMessage();
			return $book = null;
		}
	}
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try{
			if(!empty($book->title) && !empty($book->author)){
				$query=$this->db->prepare('INSERT INTO books (Title, Author, Description) VALUES(:title, :auth, :desc)');
				$query->bindValue(':title', $book->title);
				$query->bindValue(':auth', $book->author);
				$query->bindValue(':desc', $book->description);
				$query->execute();
				$book->id=$this->db->lastInsertId();
			}else{
				echo "Title and author can't be empty";
				$view = new ErrorView();
				$view->create();
			}
			
		}
		catch(Exception $e) {
			echo "Feil ved tillegging av bok";
			$e->getMessage();
			$view = new ErrorView();
				$view->create();
			
		}		
		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		try{
			$book->title=$_POST['title'];
			$book->author=$_POST['author'];
			$book->description=$_POST['description'];
			if(!empty($book->title) && !empty($book->author)){
				$query=$this->db->prepare('UPDATE books SET Title=:title, Author =:auth, Description=:desc WHERE ID = :id');
				$query->bindValue(':title', $book->title);
				$query->bindValue(':auth', $book->author);
				$query->bindValue(':desc', $book->description);
				$query->bindValue(':id', $book->id);
				$query->execute();
				$book->id=$this->db->lastInsertId();
			}else{
				echo "Title and author can't be empty";
				$view = new ErrorView();
				$view->create();
			}
			
		}
		catch(Exception $e) {
			echo "Feil ved oppdatering av bok";
			$e->getMessage();
			$view = new ErrorView();
			$view->create();			
		}		
		
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
			$query=$this->db->prepare('DELETE FROM books WHERE ID = :id');
			$query->bindValue(':id', $id);
			$query->execute();			
			
		}
		catch(Exception $e) {
			echo "Feil ved sletting av bok";
			$e->getMessage();
			$view = new ErrorView();
			$view->create();
		}	
	}
}

?>